﻿using System;
using System.Data.SqlClient;
using System.Windows;

namespace HurtownieDanych1
{
    class SQLconnector
    {
        SqlConnection connection = new SqlConnection();

        internal void Close()
        {
            connection.Close();
        }

        internal void OpenConnection()
        {
            String connectionString = "Data Source=A-KUKU-2;Initial Catalog=HurtowniaLab;Integrated Security=True";
            connection = new SqlConnection(connectionString);
            connection.Open();
        }

        internal void  Run(string commandText)
        {
            SqlCommand command = new SqlCommand(commandText, connection);
            try
            {
                int wyn = command.ExecuteNonQuery(); //uruchomienie polecenia
            }
            catch (System.Data.SqlClient.SqlException e)
            {
                MessageBox.Show(e.Message + "\n" + e.Source.ToString());

            }
        }

        internal bool fileExist(string sciezka)
        {
            string sql = "SELECT COUNT(ID) FROM AddedFiles WHERE filename = '" + sciezka + "'";
            SqlCommand command = new SqlCommand(sql, connection);
            try
            {
                int result = (int) command.ExecuteScalar();
                return (result > 0);
            }
            catch (Exception ex)
            {
                //jak się nie uda, najwyżej doda do bazki
            }

            return false;
        }
    }
}

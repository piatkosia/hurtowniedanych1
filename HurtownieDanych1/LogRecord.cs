﻿using System;

namespace HurtownieDanych1
{
    public class LogRecord
    {
        //type,date,time,source,destination,transport
        //Warto byłoby pomyśleć później nad innym przedstawieniem ww. danych
        public string type { get; set; }
        public string date { get; set; }
        public DateTime timedate { get; set; }
        public string time { get; set; }
        public string source { get; set; }
        public string destination { get; set; }
        public string transport { get; set; }
        public LogRecord(string line) {
            string[] stringrecord = line.Split(',');
            //type,date,time,source,destination,transport
            type = stringrecord[0];
            date = stringrecord[1];
            time = stringrecord[2];
            source = stringrecord[3];
            destination = stringrecord[4];
            transport = stringrecord[5];
            try
            {
                string[] datetmp = date.Split('/');
                string[] timetmp =time.Substring(0, 8).Split(':');
                timedate = new DateTime(Int32.Parse(datetmp[0]), Int32.Parse(datetmp[1]), Int32.Parse(datetmp[2]), Int32.Parse(timetmp[0]), Int32.Parse(timetmp[1]), Int32.Parse(timetmp[2]), DateTimeKind.Local);
            }
            catch (Exception e)
            {
                timedate = DateTime.Now;
            }
        }

    }
}

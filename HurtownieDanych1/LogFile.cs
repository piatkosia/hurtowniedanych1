﻿using System.Collections.ObjectModel;
namespace HurtownieDanych1
{
    public class LogFile : ObservableCollection<LogRecord>
    {
        public string ClientBanner;
        public string OperatingSystem;
        public string fileStructure;
        public ObservableCollection<LogRecord> logs;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HurtownieDanych1
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class About : Window
    {
        public About()
        {
            InitializeComponent();
            wtf.Text = "Praca zaliczeniowa z Hurtowni danych\n";
            wtf.Text += "Wykonanie: Angelika Maria Piątkowska\n";
            wtf.Text += "2013/2014 gr: 141IDU nr 72137";
        }
    }
}

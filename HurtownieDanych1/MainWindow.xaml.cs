﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Forms.VisualStyles;
using System.Windows.Threading;
using WinForms = System.Windows.Forms;
namespace HurtownieDanych1
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private bool isTaskRunning = false;
        private int iloscplikow = 0;
        SQLconnector baza = new SQLconnector();
        BackgroundWorker bw;
        /// <summary>
        /// Lista ścieżek
        /// </summary>
        List<string> rgFiles;
        /// <summary>
        /// Lista plików już sparsowanych
        /// </summary>
        ObservableCollection<string> fulltextsource = new ObservableCollection<string>();
        ObservableCollection<LogRecord> rekordyzpliku = new ObservableCollection<LogRecord>();
        //List<LogFile> pliki = new List<LogFile>();
        DispatcherTimer timer1 = new DispatcherTimer();
        int count { get; set; }
        private bool newapp = true;
        public MainWindow()
        {
            InitializeComponent();
            rekordy.DataContext = rekordyzpliku;
            FullText.DataContext = fulltextsource;
            
         
        }
        private void fileButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFolder();

            //OpenSingleFile();
        }

        public void OpenFolder()
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            pathFile.Text = dialog.SelectedPath;
            Properties.Settings.Default.CheckFolder = dialog.SelectedPath;
            GetValue(pathFile.Text);
        }

        public void GetValue(string path)
        {
            rgFiles = new List<string>(Directory.EnumerateFiles(path, "*.txt", SearchOption.AllDirectories));
        }

        public void OpenSingleFile()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".txt";
            dlg.Filter = "Plik tekstowy z logami (.txt)|*.txt";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                string filename = dlg.FileName;
                pathFile.Text = filename;
            }
        }

        private void readfile_Click(object sender, RoutedEventArgs e)
        {
                if (pathFile.Text == string.Empty)
                {
                    MessageBox.Show("Brak folderu wejściowego");
                    return;
                }
                pasek.Value = 0;
                pasek.Maximum = 100;
                bw = new BackgroundWorker();
                bw.WorkerReportsProgress = true;
                bw.WorkerSupportsCancellation = true;
                bw.DoWork += bw_DoWork;
                bw.ProgressChanged += bw_ProgressChanged;
                bw.RunWorkerCompleted += bw_RunWorkerCompleted;
                bw.RunWorkerAsync();
                readfile.IsEnabled = false;
            }
            

        void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            isTaskRunning = false;
            if (e.Cancelled == true || bw.CancellationPending == true)
            {
                MessageBox.Show("Anulowano");
                return;
            }
            else
            {
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, (Action)(() =>
                {
                    readfile.IsEnabled = true;
                }));
                
            }
        }

        void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
                pasek.Value = e.ProgressPercentage;
        }
        void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            isTaskRunning = true;
            if (bw.CancellationPending)
            {
                e.Cancel = true;
                return;
            }
            baza.OpenConnection();
            count = 0;
            przetworz();            
            baza.Close();
        }

        private void przetworz()
        {
            LiczLinie();
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, (Action)(() =>
            {
                if (newapp) ClearFields();
                newapp = false;
                pasek.Maximum = rgFiles.Count() -1;
            }));
            int ilosc_sciezek = rgFiles.Count();
            int aktualna = 0;
            foreach (string sciezka in rgFiles)
            {
                if (bw.CancellationPending == false)
                {
                    ReadThisFile(sciezka);
                    bw.ReportProgress(aktualna);
                    LiczLinie();
                    aktualna++;
                    if (aktualna == ilosc_sciezek) MessageBox.Show("Koniec!");
                    
                } 
               
            }
        }

        private void LiczLinie()
        {
            try
            {
                count = rekordyzpliku.Count();
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, (Action)(() =>
                {
                    CounterShow.Content = count.ToString();
                    FileCountrShow.Content = iloscplikow.ToString();
                }));
            }
            catch (Exception e) { Debug.Print(e.Message); }
        }

        private void ClearFields()
        {
            fulltextsource.Clear();
            rekordyzpliku.Clear();
        }

        private void ReadThisFile(string sciezka)
        {
            if (baza.fileExist(sciezka) == true)
            {
                Debug.WriteLine("Pominęło: " +  sciezka);
                return;
            }
            iloscplikow++;
            //todo(piatkosia): tutaj muszę wstrzelić sprawdzenie, czy jest w bazce
            baza.Run("Insert into AddedFiles values('"+ sciezka +"');" );
            string line;
            var file = new System.IO.StreamReader(sciezka);
            int index = 0;
            List<LogRecord> tmprecord = new List<LogRecord>();
                while ((line = file.ReadLine()) != null)
                {
                    Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, (Action)(() =>
                    {
                        LiczLinie();
                        fulltextsource.Add(line);
                    }));
                    if (index > 3)
                    {
                        LogRecord newrecord;
                        if (line.Count(x => x == ',') == 5)
                        {
                            newrecord = new LogRecord(line);
                            tmprecord.Add(newrecord);
                            SaveRecord(newrecord);
                        }
                        else
                            Debug.WriteLine("Linijka nie ma 5 przecinków. Opuszczam");
                    }
                    index++;
                }
            //foreach (var record in tmprecord)
            //{
            //    SaveRecord(record);
            //}
                file.Close();
            index = 0;
        }

 

        private void SaveRecord(LogRecord newrecord)
        {
            //SaveToDatabase(newrecord);
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, (Action) (() => { rekordyzpliku.Add(newrecord); }));
        }

        private void SaveToDatabase(LogRecord newrecord)
        {
            try
            {
                string commandText = "Insert into ZoneAlarmLog(Zdarzenie, Data, Czas, Source, Destination, Transport)  values('" + newrecord.type + "','" + newrecord.timedate + "','" + newrecord.timedate + "','" + newrecord.source + "','" + newrecord.destination + "','" + newrecord.transport + "');";
                baza.Run(commandText);
            }
            catch (Exception e)
            {
                Debug.Print("Błąd typu " + e.GetType().ToString() + e.Message);
            }
            }
        


        private void AboutButton_Click(object sender, RoutedEventArgs e)
        {
            About about = new About();
            about.Show();
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            bw.CancelAsync();
            bw.Dispose();
            ClearFields();
            pasek.Value = 0;
            rekordyzpliku.Clear();
            pathFile.Text = string.Empty;
            iloscplikow = 0;
            FileCountrShow.Content = string.Empty;
            CounterShow.Content = string.Empty;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            if (bw.IsBusy == true)
            {
                bw.CancelAsync();
                LiczLinie();
                MessageBox.Show("Anulowano");
                Application.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;
            }
                
            else 
            MessageBox.Show("Nic nie anulowało");
        }

        void Current_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            Debug.WriteLine(e.Exception.Message);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void update_Click(object sender, RoutedEventArgs e)
        {
            UpdateSet UpdateWindow = new UpdateSet();
            UpdateWindow.Closed += UpdateWindow_Closed;
            UpdateWindow.Show();
            timer1.Tick += timer1_Tick;
        }

        void timer1_Tick(object sender, EventArgs e)
        {
            if (isTaskRunning == false)
            {
                bw.RunWorkerAsync();
                isTaskRunning = true;
            }
        }

        void UpdateWindow_Closed(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UpdateEnable)
            {
                GetValue(Properties.Settings.Default.CheckFolder);
                timer1.Interval = new TimeSpan(0, 0, 0, Properties.Settings.Default.UpdateInterval);
                timer1.Start();
            }
            else
            {
                timer1.IsEnabled = false;
                timer1.Stop();
            }

        }
    }
    }

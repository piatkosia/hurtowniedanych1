﻿using System.Data;
using System.Windows;

namespace HurtownieDanych2
{
    /// <summary>
    /// Interaction logic for ChartWindow.xaml
    /// </summary>
    public partial class ChartWindow : Window
    {
        public ChartWindow()
        {
            InitializeComponent();
        }

        internal void wypelnij(DataTable tabelka)
        {
            columnChart.DataContext = tabelka.DefaultView;
        }
    }
}

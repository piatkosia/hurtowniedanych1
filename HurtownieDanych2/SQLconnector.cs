﻿using System;
using System.Data.SqlClient;
using System.Windows;
using System.Data;

namespace HurtownieDanych2
{
    class SQLconnector
    {
        SqlConnection connection = new SqlConnection();
        String connectionString = "Data Source=A-KUKU-2;Initial Catalog=HurtowniaLab;Integrated Security=True";
        internal void Close()
        {
            connection.Close();
        }

        internal void OpenConnection()
        {
            connection = new SqlConnection(connectionString);
            connection.Open();
        }

        internal void Run(string commandText)
        {
            SqlCommand command = new SqlCommand(commandText, connection);
            try
            {
                int wyn = command.ExecuteNonQuery(); //uruchomienie polecenia
            }
            catch (System.Data.SqlClient.SqlException e)
            {
                MessageBox.Show(e.Message + "\n" + e.Source.ToString());

            }
        }
        internal DataTable getData(string sqlstring)
        {
            DataTable table = new DataTable("Wyniki");
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlDataAdapter dataAdapter = new SqlDataAdapter(new SqlCommand(sqlstring, con));
                dataAdapter.Fill(table);
            }
            return table;
        }
    }
}

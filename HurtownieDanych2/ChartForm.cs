﻿using System;
using System.Data;
using System.Windows.Forms;

namespace HurtownieDanych2
{
    public partial class ChartForm : Form
    {
        public ChartForm()
        {
            InitializeComponent();
        }
        public ChartForm(DataTable tabelka)
        {
            InitializeComponent();

            chart1.DataBindTable(tabelka.DefaultView, "Zdarzenie");
            chart1.Series["ile"].Name = "Liczba zdarzeń wg typu";
            chart2.DataBindTable(tabelka.DefaultView, "Zdarzenie");
            chart2.Series["ile"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
        }

        private void chart1_Click(object sender, EventArgs e)
        {
        }

        private void ChartForm_Load(object sender, EventArgs e)
        {

        }
    }
}

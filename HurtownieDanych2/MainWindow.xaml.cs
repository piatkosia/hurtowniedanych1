﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;
using Microsoft.Office.Interop.Excel;
using Application = Microsoft.Office.Interop.Excel.Application;
using DataTable = System.Data.DataTable;
using Window = System.Windows.Window;

namespace HurtownieDanych2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DateTime from;
        DateTime to;
        DataTable tabelka;
        SQLconnector sqlconnector = new SQLconnector();
        public MainWindow()
        {
            InitializeComponent();
            from_date.Watermark = "Początek";
            to_date.Watermark = "Koniec";
        }

        private void EndButton(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void DateFilterButton(object sender, RoutedEventArgs e)
        {
            if (from_date.Value > to_date.Value)
            {
                MessageBox.Show("Data rozpoczęcia wcześniejsza od daty zakończenia");
                from_date.Value = null;
                to_date.Value = null;
            }
            else
            {
                FillData("Select * from ZoneAlarmLog where Data between '" + from_date.Value + "' and '" + to_date.Value +"'");
            }
        }

        private void CountAllClick(object sender, RoutedEventArgs e)
        {
            FillData("Select count(*) As Wszystkie from ZoneAlarmLog");
   
        }

        private void FillData(string cmd)
        {
            DataTable data = new DataTable();
            data = sqlconnector.getData(cmd);
            dane.ItemsSource = data.DefaultView;
            tabelka = new DataTable();
            tabelka.Reset();
            tabelka = data;
        }

        private void ShowAllClick(object sender, RoutedEventArgs e)
        {
            FillData("Select * from ZoneAlarmLog");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            FillData("Select count(id) as ile, Zdarzenie from ZoneAlarmLog group by Zdarzenie");
            ChartForm okienko = new ChartForm(tabelka);
            okienko.Show();
            //ChartWindow okienko = new ChartWindow();
        }

        private void CountMax(object sender, RoutedEventArgs e)
        {
            FillData("SELECT TOP 1 convert(date, DATEADD(day, DATEDIFF(day, 0, Data), 0),120)  AS kiedy, COUNT(id) AS Count FROM ZoneAlarmLog GROUP BY DATEADD(day, DATEDIFF(day, 0, Data), 0) ORDER BY Count desc");
        }

        private void ToExcel_Click(object sender, RoutedEventArgs e)
        {
            Application apps;
            Workbook workbook;
            Worksheet arkusz;
            object misValue = Missing.Value;
            Int16 i, j;
            apps = new Application();
            workbook = apps.Workbooks.Add(misValue);
            arkusz = workbook.Sheets[1];
            var columns = tabelka.Columns.Count;
            var rows = tabelka.Rows.Count;
            Range range = arkusz.Range["A1", String.Format("{0}{1}", GetExcelColumnName(columns), rows)];
            object[,] data = new object[rows, columns];
            for (int rowNumber = 0; rowNumber < rows; rowNumber++)
            {
                for (int columnNumber = 0; columnNumber < columns; columnNumber++)
                {
                    data[rowNumber, columnNumber] = tabelka.Rows[rowNumber][columnNumber].ToString();
                }
            }

            range.Value = data;
            //workbook.SaveCopyAs(@"E:\test\backup.xlsx");
            workbook.Close(); //przy zamykaniu sam poprosi o zapis - nie ma sensu samodzielnie robić kopii
            Marshal.ReleaseComObject(apps);
        }
        private static string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }

        private void years_Click(object sender, RoutedEventArgs e)
        {
            FillData("SELECT YEAR(Data) AS Rok,Count(Data)  AS IleRekordow FROM dbo.ZoneAlarmLog GROUP BY YEAR(Data) ORDER BY IleRekordow DESC");
        }

        private void byday_Click(object sender, RoutedEventArgs e)
        {
            FillData("SELECT Year(Data) as 'Rok',Month(Data) as 'Miesiac',Day(Data) AS 'Dzien', Count(Data)  AS IleRekordow FROM dbo.ZoneAlarmLog GROUP BY Year(Data),Month(Data),Day(Data) ORDER BY IleRekordow DESC");
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            FillData("SELECT Year(Data) as 'Rok',Month(Data) as 'Miesiac',Count(Data)  AS IleRekordow FROM dbo.ZoneAlarmLog GROUP BY Year(Data),Month(Data) ORDER BY IleRekordow  desc");
        }
    }
}
